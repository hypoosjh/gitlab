export const VERSION_CHECK_BADGE_NO_PROP_FIXTURE =
  '<div class="js-gitlab-version-check-badge"></div>';

export const VERSION_CHECK_BADGE_NO_SEVERITY_FIXTURE = `<div class="js-gitlab-version-check-badge" data-version='{ "size": "sm" }'></div>`;

export const VERSION_CHECK_BADGE_FIXTURE = `<div class="js-gitlab-version-check-badge" data-version='{ "severity": "success" }'></div>`;
